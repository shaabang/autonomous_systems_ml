# Lab 4 statement

The objective of this lab is to illustrate support vector machine (part I) and clustering methods (part II)

_Note: For each notebook, read the cells and run the code, then follow the instructions/questions in the `Questions` or `Exercise` cells._


## Part I

This part illustrates SVM for both linear and non-linear classification tasks. This also introduces
one-class SVM, an extension of the SVM principle to the novelty detection for unuspervised learning.


See the notebooks in the [7_support_vector_machine ](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/7_support_vector_machine/) folder:


1. Run and plot the maximum margin separating hyperplane within a two-class separable or not dataset and interpret the role of the kernel and SVM parameters [`N1_plot_svm.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/7_support_vector_machine/N1_plot_svm.ipynb).

2. Perform SVM classification for some pairs of (zip code) digits that are the hardest to discriminate using a linear discriminant analysis method [`N2_svm_zip_digits.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/7_support_vector_machine/N2_svm_zip_digits.ipynb).


The next notebooks are **optional**. But if you want to go deeper, just take a look at it during or after the class session

3. *Optional*: Train and plot one-class SVM algorithm to classify new data as *similar* or *different* to the training set on a toy problem
 [`N3_oneclasssvm_novelty_detection.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/7_support_vector_machine/N3_oneclasssvm_novelty_detection.ipynb).

4. *Optional*: Experiment the importance of scaling on a toy example for both linear and non-linear SVMs [`N4_importance_of_scaling-svm.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/7_support_vector_machine/N4_importance_of_scaling-svm.ipynb)

<!--
5. *Optional*:  Apply a simple dimension reduction, here PCA, to improve  the performance while reducing the computational burden of the SVM classifier on real faces data [`N5_svm_face_recognition.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/7_support_vector_machine/N5_svm_face_recognition.ipynb).
-->

## Part II 


We illustrate here basic concepts of clustering.


See the notebooks in the [`8_Clustering`](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/8_Clustering/) folder


1. Implement your own version of Kmeans in 1D for Euclidean distance  [`N1_Kmeans_basic.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/8_Clustering/N1_Kmeans_basic.ipynb)
and compare the obtained results with Sklearn Kmeans implementation.

2. Apply Kmeans algorithm (Sklearn implementation) to the classical Iris dataset [`N2_KMeans_iris_data_example.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/8_Clustering/N2_KMeans_iris_data_example.ipynb)

3. Implement a Kernelized version of Kmeans, and test the importance of adequate parametrization or choice of initial conditions
 [`N3_Kernel_Kmeans_example.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/8_Clustering/N3_Kernel_Kmeans_example.ipynb)

4.  Implement your own version of EM for Gaussian model  and apply it to the same example used for Kmeans in a preceeding notebook. Compare with KMeans and interpret the results [`N4_EM_basic.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/8_Clustering/N4_EM_basic.ipynb)

5. Example of EM application on the Iris data set [`N5_EM_iris_data_example.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/ai-courses/autonomous_systems_ml/-/blob/master/notebooks/8_Clustering/N5_EM_iris_data_example.ipynb)
